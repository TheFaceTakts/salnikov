#ifndef _FIELD_H
#define _FIELD_H

#include "constants.hpp"
#include "configuration.hpp"
#include "functions.hpp"

#include <algorithm>
#include <cassert>
#include <cstdio>

using std::min;


struct Cell {
    uint32 food;
    uint32 substance[2];
    uint32 starving;
    mybool bacteria[2];
};


const uint32 FOOD_PROP = 0;
const uint32 SUBSTANCE_0_PROP = 1;
const uint32 SUBSTANCE_1_PROP = 2;
const uint32 STARVING_PROP = 3;
const uint32 BACTERIA_0_PROP = 4;
const uint32 BACTERIA_1_PROP = 5;
const uint32 PROP_CNT = 6;


struct Field {
    struct Move {
        int dx, dy;
    };
    uint32 MOVES_COUNT;
    Move moves[8];

    Cell field[MAX_SIZE][MAX_SIZE];
    uint32 N, M;

    uint32* generationOfDown;

    uint32* generationOfUp;

    void initMoves() {
        MOVES_COUNT = 0;
        for (int i = -1; i <= 1; ++i) {
            for (int j = -1; j <= 1; ++j) {
                if (i || j) {
                    moves[MOVES_COUNT].dx = i;
                    moves[MOVES_COUNT].dy = j;
                    MOVES_COUNT++;
                }
            }
        }
    }

    void setWorkers(uint32 workers) {
        generationOfDown = new uint32[workers];
        generationOfUp = new uint32[workers];
        for (int i = 0; i < workers; ++i) {
            generationOfDown[i] = 0;
            generationOfUp[i] = 0;
        }
    }

    Field() {
        initMoves();
    }

    ~Field() {
        delete[] generationOfUp;
        delete[] generationOfDown;
    }
};

#endif
