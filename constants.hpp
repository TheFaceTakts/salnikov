#ifndef _CONSTANTS_H
#define _CONSTANTS_H

typedef unsigned int uint32;
typedef int mybool;

const uint32 MAX_SIZE = 1000;
const uint32 MAX_BUFF = 1000;

const char* INPUT_FIELD_FILENAME = "state.txt";
const char *CONFIGURATION_FILENAME = "creatures.conf";
const char *OUTPUT_FIELD_FILENAME = "final_state.txt";

#endif
