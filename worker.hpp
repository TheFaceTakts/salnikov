#ifndef _WORKER_H
#define _WORKER_H

#include "constants.hpp"
#include "functions.hpp"
#include "configuration.hpp"
#include "field.hpp"

#include <algorithm>
#include <cassert>
#include <cstdio>

using std::min;


struct Rand {
    static const uint32 A = 18923, B = 9182738, C = 1000000000 + 7;

    uint32 seed;

    uint32 getRand() {
        return (seed = (A * seed + B) % C);
    }

    uint32 getRand(uint32 from, uint32 to) {
        return getRand() % (to - from) + from;
    }

    Rand(uint32 seed) : seed(seed) {
    }
};


struct Worker {
    const Configuration& conf;
    Rand rand;
    Field &field;
    uint32 from, to;
    uint32 steps;

    uint32 prevWorker, nextWorker, meWorker;

    Worker(Field &field, uint32 from, uint32 to, uint32 steps,
           uint32 number, uint32 workers) :
        field(field), conf(Configuration::getConf()),
        from(from), to(to),
        rand(from), steps(steps),
        prevWorker((number - 1 + workers) % workers),
        meWorker(number),
        nextWorker((number + 1) % workers) {

    }

    Cell &getCell(int x, int y) {
        if (x < 0) {
            x += field.N;
        }
        if (x >= field.N) {
            x -= field.N;
        }
        if (y < 0) {
            y += field.M;
        }
        if (y < 0) {
            y += field.M;
        }
        return field.field[x][y];
    }

    uint32 getCellProp(int x, int y, uint32 prop) {
        Cell &cell = getCell(x, y);
        switch (prop) {
            case FOOD_PROP:
                return cell.food;
            break; case SUBSTANCE_0_PROP:
                return cell.substance[0];
            break; case SUBSTANCE_1_PROP:
                return cell.substance[1];
            break; case STARVING_PROP:
                return cell.starving;
            break; case BACTERIA_0_PROP:
                return cell.bacteria[0];
            break; case BACTERIA_1_PROP:
                return cell.bacteria[1];
            break;
        }
        fprintf(stderr, "Wrong prop %u\n", prop);
        assert(false);
        return 0u;
    }

    void setCellProp(int x, int y, uint32 prop, uint32 value) {
        Cell &cell = getCell(x, y);
        switch (prop) {
            case FOOD_PROP:
                cell.food = value;
            break; case SUBSTANCE_0_PROP:
                cell.substance[0] = value;
            break; case SUBSTANCE_1_PROP:
                cell.substance[1] = value;
            break; case STARVING_PROP:
                cell.starving = value;
            break; case BACTERIA_0_PROP:
                cell.bacteria[0] = value;
            break; case BACTERIA_1_PROP:
                cell.bacteria[1] = value;
            break; default:
                fprintf(stderr, "Wrong prop %u\n", prop);
                assert(false);
        }
    }

    void changeCellProp(int x, int y, uint32 prop, uint32 value) {
        Cell &cell = getCell(x, y);
        switch (prop) {
            case FOOD_PROP:
                cell.food += value;
            break; case SUBSTANCE_0_PROP:
                cell.substance[0] += value;
            break; case SUBSTANCE_1_PROP:
                cell.substance[1] += value;
            break; case STARVING_PROP:
                cell.starving += value;
            break; case BACTERIA_0_PROP:
                cell.bacteria[0] += value;
            break; case BACTERIA_1_PROP:
                cell.bacteria[1] += value;
            break; default:
                fprintf(stderr, "Wrong prop %u\n", prop);
                assert(false);
        }
    }

    void consumeSubstance(uint32 x, uint32 y, uint32 s, uint32 needed) {
        Cell &cell = field.field[x][y];
        uint32 neededCopy = needed;
        if (s == FOOD_PROP) {
            needed -= min(needed, cell.food);
            cell.food -= min(cell.food, neededCopy);
        } else {
            needed -= min(needed, cell.substance[s - SUBSTANCE_0_PROP]);
            cell.substance[s - SUBSTANCE_0_PROP] -=
                min(cell.substance[s - SUBSTANCE_0_PROP], neededCopy);
        }
        if (needed) {
            for (int i = 0; i < field.MOVES_COUNT; ++i) {
                changeCellProp(x + field.moves[i].dx, y + field.moves[i].dy,
                    -(needed / field.MOVES_COUNT +
                      (i < needed % field.MOVES_COUNT)),
                    s);
            }
        }
    }

    void split(uint32 x, uint32 y, uint32 type) {
        uint32 notAlive = 0;
        for (int i = 0; i < field.MOVES_COUNT; ++i) {
            notAlive += (1 -
                         getCellProp(x + field.moves[i].dx,
                                     y + field.moves[i].dy,
                                     BACTERIA_0_PROP + type));
        }
        if (notAlive == 0) {
            return;
        }
        uint32 luckyGuy = rand.getRand(0, notAlive);
        for (int i = 0; i < field.MOVES_COUNT; ++i) {
            if (!getCellProp(x + field.moves[i].dx, y + field.moves[i].dy,
                             BACTERIA_0_PROP + type)) {
                if (luckyGuy == 0) {
                    setCellProp(x + field.moves[i].dx, y + field.moves[i].dy,
                                BACTERIA_0_PROP + type, 1u);
                }
                luckyGuy -= 1;
            }
        }
    }

    uint32 getSubstanceAround(uint32 x, uint32 y, uint32 s) {
        uint32 answer = 0;
        for (int i = 0; i < field.MOVES_COUNT; ++i) {
            answer += getCellProp(x + field.moves[i].dx,
                                  y + field.moves[i].dy, s);
        }
        return answer;
    }

    void makeStep(uint32 x, uint32 y, uint32 current) {
        Cell &cell = field.field[x][y];
        if (current == 0) {
            if (!cell.bacteria[0]) {
                return;
            }
            uint32 additionalSubstance =
                getSubstanceAround(x, y, SUBSTANCE_1_PROP);
            if (cell.substance[0] > conf.CRITICAL_SUBSTANCE ||
                cell.substance[1] + additionalSubstance <
                                                conf.NEEDED_SUBSTANCE[0]) {
                cell.bacteria[0] = 0;
                cell.starving = 0;
                return;
            }
            uint32 additionalFood = getSubstanceAround(x, y, FOOD_PROP);

            if (cell.food + additionalFood < conf.FOOD_EAT_SPEED) {
                if (cell.starving >= conf.STARVATION_TIME) {
                    cell.bacteria[0] = 0;
                    cell.starving = 0;
                    return;
                }
                if (cell.substance[1] + additionalSubstance
                        >= conf.STARVATION_SUBSTANCE_EAT_SPEED) {
                    ++cell.starving;
                    consumeSubstance(x, y, 1,
                        conf.STARVATION_SUBSTANCE_EAT_SPEED);
                } else {
                    cell.bacteria[0] = 0;
                    cell.starving = 0;
                    return;
                }
            } else {
                cell.starving = 0;
                consumeSubstance(x, y, FOOD_PROP, conf.FOOD_EAT_SPEED);
                consumeSubstance(x, y, SUBSTANCE_1_PROP,
                        conf.SUBSTANCE_EAT_SPEED[0]);
            }
            if (cell.substance[0] == conf.SPLIT_CONCENTRATIONS[0][0] &&
                cell.substance[1] == conf.SPLIT_CONCENTRATIONS[0][1] &&
                cell.food == conf.SPLIT_CONCENTRATIONS[0][2]) {
                split(x, y, 0u);
            }
            cell.substance[0] += conf.SUBSTANCE_PRODUCE_SPEED[0];
        } else {
            if (!cell.bacteria[1]) {
                return;
            }
            uint32 additionalSubstance = getSubstanceAround(x, y,
                                            SUBSTANCE_0_PROP);
            if (cell.substance[0] + additionalSubstance <
                    conf.SUBSTANCE_EAT_SPEED[1] ||
                cell.substance[0] < conf.NEEDED_SUBSTANCE[1]) {
                cell.bacteria[1] = 0;
                return;
            }
            if ((cell.substance[0] == conf.SPLIT_CONCENTRATIONS[0][0] &&
                cell.substance[1] == conf.SPLIT_CONCENTRATIONS[0][1] &&
                cell.food == conf.SPLIT_CONCENTRATIONS[0][2]) ||
                cell.substance[0] >= conf.BIRTH_CONCENTRATION) {
                split(x, y, 1u);
            }
            cell.substance[1] += conf.SUBSTANCE_PRODUCE_SPEED[1];
        }
    }

    void makeStep(uint32 x, uint32 y) {
        Cell &cell = field.field[x][y];
        cell.food = getNewFood(x, y, cell.food, steps);
        uint32 current = rand.getRand(0, 2);
        makeStep(x, y, current);
        makeStep(x, y, 1 ^ current);
    }

    void makeStep(uint32 i) {
        for (int j = 0; j < field.M; ++j) {
            makeStep(i, j);
        }
    }

    uint32 lastNotMade;

    bool makeUnimportantStep() {
        if (lastNotMade == to - 2) {
            return true;
        }
        makeStep(lastNotMade++);
        return false;
    }

    void makeStepNumber(uint32 step) {
        lastNotMade = from + 2;
        while (true) {
            if (field.generationOfUp[meWorker] != step) {
                if (field.generationOfDown[prevWorker] == step - 1) {
                    makeStep(from);
                    ++field.generationOfUp[meWorker];
                }
            }
            if (field.generationOfDown[meWorker] != step) {
                if (field.generationOfUp[nextWorker] == step) {
                    makeStep(to - 1);
                    ++field.generationOfDown[meWorker];
                }
            }
            if (field.generationOfUp[meWorker] == step &&
                field.generationOfDown[meWorker] == step) {
                break;
            }
            makeUnimportantStep();
        }
        makeStep(from + 1);
        makeStep(to - 2);
        while (!makeUnimportantStep());
    }

    void run() {
        for (uint32 i = 1; i <= steps; ++i) {
            makeStepNumber(i);
        }
    }
};

#endif
