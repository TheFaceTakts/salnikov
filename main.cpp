#include "field.hpp"
#include "io.hpp"
#include "configuration.hpp"
#include "worker.hpp"

#include <omp.h>
#include <time.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <map>
#include <string>
#include <vector>

using std::string;
using std::map;
using std::vector;

Field field;
const Configuration &conf = Configuration::getConf();

void makeArgs(uint32 p, vector<uint32> &from, vector<uint32> &to, uint32 n) {
    from.resize(p);
    to.resize(p);

    uint32 start = 0, end = 0;
    for (uint32 i = 0; i < p; ++i) {
        start = end;
        end = start + (n / p) + (i < (n % p));
        from[i] = start;
        to[i] = end;
    }
}

int main(int argc, char** argv) {
    int proc = omp_get_num_procs();
    fprintf(stderr, "Max proc %d\n", proc);
    map<string, uint32> arguments;
    for (int i = 1; i < argc; i += 2) {
        string argument(argv[i] + 2);
        arguments[argument] = atoi(argv[i + 1]);
        printf("%s, %d\n", argument.c_str(), arguments[argument]);
    }
    if (!readInput("state.txt", field)) {
        return 1;
    }

    uint32 p = arguments["threads"];
    p = min(field.N / 5, p);
    uint32 steps = arguments["steps"];
    vector <uint32> from, to;
    makeArgs(p, from, to, field.N);

    field.setWorkers(p);

    struct timespec start, finish;
    double elapsed;

    clock_gettime(CLOCK_MONOTONIC, &start);

    #pragma omp parallel num_threads (p)
    {
        uint32 i = omp_get_thread_num();
        Worker worker(field, from[i], to[i], steps, i, p);
        worker.run();
    }

    clock_gettime(CLOCK_MONOTONIC, &finish);

    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

    fprintf(stderr, "Time: %.3lf\n", elapsed);
    printFieldFile(field, OUTPUT_FIELD_FILENAME);
}
