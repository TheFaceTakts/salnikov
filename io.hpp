#ifndef _IO_H
#define _IO_H

#include "constants.hpp"
#include "field.hpp"

#include <cstdio>
#include <cstring>

char DA[2] = {'D', 'A'};

mybool readInput(const char *filename, Field &field) {
    FILE *file = fopen(filename, "r");
    char buff[MAX_BUFF];
    mybool readDimentions = false;
    uint32 line = 0;
    if (file != NULL) {
        while (fgets(buff, MAX_BUFF - 1, file)) {
            ++line;
            if (buff[0] == '#' || buff[0] == '\n' || buff[0] == ' ') {
                continue;
            }
            if (!readDimentions) {
                char area[10];
                if (sscanf(buff, "%s %u x %u", area, &field.N, &field.M) == 3) {
                    if (!strcmp(area, "Area") &&
                        field.N < MAX_SIZE && field.M < MAX_SIZE) {
                        buff[0] = '#';
                        readDimentions = true;
                        continue;
                    }
                }
                fprintf(stderr, "Bad input file (Area stuff), line %u\n", line);
                fprintf(stderr, buff);
                return false;
            }
            uint32 x, y;
            Cell cell;
            if (sscanf(buff, "%u %u %u %u %u %u %u %u",
                        &x, &y, &cell.food, &cell.substance[0],
                        &cell.substance[1], &cell.bacteria[0],
                        &cell.starving, &cell.bacteria[1]) == 8) {
                if (x < field.N && y < field.M) {
                    field.field[x][y] = cell;
                } else {
                    fprintf(stderr, "Bad input file (wrong coords) line %u\n",
                            line);
                    fprintf(stderr, buff);
                    return false;
                }
            } else {
                fprintf(stderr, "Bad input file (wrong coords) line %u\n",
                        line);
                fprintf(stderr, buff);
                return false;
            }
            buff[0] = '#';
        }
        fclose(file);
        fprintf(stderr, "Read input, lines %u\n", line + 1);
        return true;
    }
    fprintf(stderr, "Bad input file (cannot open)\n");
    return false;
}

void printField(const Field &field, FILE *file) {
    fprintf(file, "%u %u\n", field.N, field.M);
    for (int i = 0; i < field.N; ++i) {
        for (int j = 0; j < field.M; ++j) {
            fprintf(file, "----");
        }
        fprintf(file, "-\n");
        for (int j = 0; j < field.M; ++j) {
            fprintf(file, "|%c %u", DA[field.field[i][j].bacteria[0]],
                                    field.field[i][j].starving);
        }
        fprintf(file, "|\n");
        for (int j = 0; j < field.M; ++j) {
            fprintf(file, "|%c %u", DA[field.field[i][j].bacteria[1]],
                                   field.field[i][j].food);
        }
        fprintf(file, "|\n");
        for (int j = 0; j < field.M; ++j) {
            fprintf(file, "|%u %u", field.field[i][j].substance[0],
                                   field.field[i][j].substance[1]);
        }
        fprintf(file, "|\n");
    }
    for (int j = 0; j < field.M; ++j) {
        fprintf(file, "----");
    }
    fprintf(file, "-\n");
}

void printFieldFile(const Field &field, const char *filename) {
    FILE *file = fopen(filename, "w");
    if (file != NULL) {
        printField(field, file);
        fclose(file);
        return;
    }
    fprintf(stderr, "Error writing field, cannot open file %s", filename);
}

#endif
