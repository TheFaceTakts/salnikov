#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

#include "constants.hpp"

#include <cstdio>
#include <cstring>
#include <cstdlib>

struct Configuration {
private:
    unsigned long long ready;

    void setReady(unsigned long long i) {
        ready |= (1LLU << i);
    }

    void reportBadFile(uint32 line) {
        fprintf(stderr, "Bad configuration file, line %u\n", line);
    }

    Configuration() {
        ready = 0;
        CORRECT_CONFIGURATION = false;
        FILE *file = fopen(CONFIGURATION_FILENAME, "r");
        char buff[MAX_BUFF];
        char command[MAX_BUFF];
        uint32 line = 0;
        if (file != NULL) {
            while (fgets(buff, MAX_BUFF - 1, file)) {
                ++line;
                if (buff[0] == '#' || buff[0] == '\n' || buff[0] == ' ') {
                    continue;
                }
                if (sscanf(buff, "%s", command) == 1) {
                    char *_buff= buff + strlen(command);
                    if (!strcmp(command, "FOOD_EAT_SPEED")) {
                        if (sscanf(_buff, " %u", &FOOD_EAT_SPEED) != 1) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(0LLU);
                    } else if (!strcmp(command, "SUBSTANCE_EAT_SPEED")) {
                        if (sscanf(_buff, "%u %u", &SUBSTANCE_EAT_SPEED[0],
                                                   &SUBSTANCE_EAT_SPEED[1])
                            != 2) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(1LLU);
                    } else if (!strcmp(command, "SUBSTANCE_PRODUCE_SPEED")) {
                        if (sscanf(_buff, "%u %u", &SUBSTANCE_PRODUCE_SPEED[0],
                                                   &SUBSTANCE_PRODUCE_SPEED[1])
                            != 2) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(2LLU);
                    } else if (!strcmp(command, "NEEDED_SUBSTANCE")) {
                        if (sscanf(_buff, "%u %u", &NEEDED_SUBSTANCE[0],
                                                   &NEEDED_SUBSTANCE[1])
                            != 2) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(3LLU);
                    } else if (!strcmp(command, "NEEDED_FOOD")) {
                        if (sscanf(_buff, "%u", &NEEDED_FOOD) != 1) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(4LLU);
                    } else if (!strcmp(command, "CRITICAL_SUBSTANCE")) {
                        if (sscanf(_buff, "%u", &CRITICAL_SUBSTANCE) != 1) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(5LLU);
                    } else if (!strcmp(command, "STARVATION_TIME")) {
                        if (sscanf(_buff, "%u", &STARVATION_TIME) != 1) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(6LLU);
                    } else if (!strcmp(command,
                                       "STARVATION_SUBSTANCE_EAT_SPEED")) {
                       if (sscanf(_buff, "%u", &STARVATION_SUBSTANCE_EAT_SPEED)
                           != 1) {
                           reportBadFile(line);
                           return;
                       }
                       setReady(7LLU);
                    } else if (!strcmp(command, "SPLIT_CONCENTRATIONS")) {
                        if (sscanf(_buff, "%u %u %u %u %u %u",
                                &SPLIT_CONCENTRATIONS[0][0],
                                &SPLIT_CONCENTRATIONS[0][1],
                                &SPLIT_CONCENTRATIONS[0][2],
                                &SPLIT_CONCENTRATIONS[1][0],
                                &SPLIT_CONCENTRATIONS[1][1],
                                &SPLIT_CONCENTRATIONS[1][2])
                            != 6) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(8LLU);
                    } else if (!strcmp(command, "BIRTH_CONCENTRATION")) {
                        if (sscanf(_buff, "%u", &BIRTH_CONCENTRATION) != 1) {
                            reportBadFile(line);
                            return;
                        }
                        setReady(9LLU);
                    } else {
                        fprintf(stderr,
                                "Bad configuration file, line %u, command %s\n",
                                line, command);
                        return;
                    }
                } else {
                    reportBadFile(line);
                    return;
                }
                buff[0] = '#';
            }
            fclose(file);
            fprintf(stderr, "Read configuration, lines %u\n", line + 1);
            if (ready != 0b1111111111LLU) {
                fprintf(stderr, "Bad configuration: %llu != %llu\n", ready,
                        0b1111111111LLU);
                return;
            }
            CORRECT_CONFIGURATION = true;
        } else {
            fprintf(stderr,
                    "Error while reading configuration: cannot open file\n");
        }
    }

    Configuration(Configuration const &copy);
    Configuration& operator=(Configuration const& copy);

    ~Configuration() {

    }
public:

    uint32 FOOD_EAT_SPEED;
    uint32 SUBSTANCE_EAT_SPEED[2];
    uint32 SUBSTANCE_PRODUCE_SPEED[2];
    uint32 NEEDED_SUBSTANCE[2];
    uint32 NEEDED_FOOD;
    uint32 CRITICAL_SUBSTANCE;
    uint32 STARVATION_TIME;
    uint32 STARVATION_SUBSTANCE_EAT_SPEED;
    uint32 SPLIT_CONCENTRATIONS[2][3];
    uint32 BIRTH_CONCENTRATION;

    mybool CORRECT_CONFIGURATION;

    static const Configuration& getConf() {
        static Configuration conf;
        return conf;
    }
};

#endif
